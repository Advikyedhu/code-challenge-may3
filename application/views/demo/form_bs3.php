<h1>HTML &amp; CSS<h1>
<h2>Requirements</h2>
<p><strong>Task 1: </strong></p>
<ol>
	<li>Based on the following mock up:</li>
	<p><img src="<?php echo base_url() ?>/assets/images/sample.png" alt="sample" /></p>
	<li>Create the HTML version of it</li>
	<li>You will be required to use Bootstrap to create 2 columns of 6 and it should be responsive</li>
	<li>The purpose of the colums are in the description on point 5</li>
	<li>On responsive view, the order of the columns should be: 
		<ol>
			<li>Starts (Simple HTML with the date being the current date and time)</li>
			<li>Invitees (Simple HTMl)</li>
			<li>Audio Player (use the HTML5 audio player <a href="http://www.w3schools.com/html/html5_audio.asp" target="_blank">http://www.w3schools.com/html/html5_audio.asp)</a></li>
			<li>Smartlife (Google Map integration with your current location)</li>
			<li>Location (Simple HTML)</li>
			<li>Image (Simple HTML)</li>
		</ol>
	</li>
	<li>All images will be within assets/images/</li>
</ol>

<!--Start Coding-->

<!--End Coding-->