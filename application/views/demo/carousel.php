<h1>Javascript &amp; jQuery<h1>
<h2>Requirements</h2>
<p><strong>Task 1: </strong>Using the jQuery library - <a href="http://sorgalla.com/jcarousel/examples/responsive/" target="_blank">http://sorgalla.com/jcarousel/examples/responsive/</a>, create a carousel with the required scripts all embeddded within this view. You may make use of the php loop below to loop through the images so as to populate the images. All right and left buttons should be working</p>

		<div class="wrapper">
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul>
						<?php for ($i=0; $i<count($photos); $i++): ?>						
							<li><img src="<?php echo  $photos[$i]->image_url; ?>" alt="Photo <?php echo $i+1; ?>" />	</li>						
						<?php endfor; ?>	
					</ul>
                </div>
               

                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                
                <p class="jcarousel-pagination">
                    
                </p>
            </div>
        </div>
	

<!--Start Coding-->

<!--End Coding-->

<br style="clear:both;" />

<h2>Requirements</h2>
<p><strong>Task 2: </strong> </p>
<ol>
<li>Write a XMLHttpRequest in Javascript to get the contact details of a person using the following API call</li>
<li>Method: POST</li>
<li>URL: <a href="http://im.gdbg.com:8080/commsapplib/ws/clGetContactDetails">http://im.gdbg.com:8080/commsapplib/ws/clGetContactDetails</a></li>
<li>Parameters: webId: 'daryll'</li>
<li>Sample result: <code>{"error":null,"data":{"status":"1","Contact":{"recordId":null,"mobileNumber":"96410182","othernumbers":null,"dialingCode":"65","firstName":"Daryll","lastName":"Chu","profileImage":null,"email":"daryll@gdbg.com","otheremails":null,"address":null,"birthday":null,"webId":"daryll","friend":null,"userId":null,"latitude":0.0,"longitude":0.0,"lastUpdated":null,"otherdates":{},"socialProfiles":{},"geoPoint":{"lat":null,"lng":null},"contactAddRequestStatus":null}},"result":"Success"}</code></li>
<li>Display the results of the contact (for eg. recordId, mobileNumber, otherNumbers etc) into a table with <code>th</code> with all the return values within <code>tr</code> and <code>td</code></li>
</ol>

<!--Start Coding-->

<!--End Coding-->