<?php
class Blog extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->library('form_builder');
			$this->load->model('Blog_model');
			$this->load->helper('url');
			$this->push_breadcrumb('Demo');
			$this->load->library('pagination');
		}
       public function index()
		{
			redirect('demo/item/1');
		}

		public function item($demo_id)
		{
			$this->mViewData['demo_id'] = $demo_id;
			$this->render('demo/item');
		}
		public function add_new_entry()
		{
				
				
				$this->mViewData['categories'] = $this->Blog_model->get_categories();
				
		 
				$this->load->helper('form');
				$this->load->library(array('form_validation'));
		 
				//set validation rules
				$this->form_validation->set_rules("entry_name", "Title", "required");
				$this->form_validation->set_rules("entry_body", "Body", "required");
				$this->form_validation->set_rules("entry_category", "Category", "required");
				$body = $this->input->post('entry_body');	
				if (isset($body))
				{
					
					//var_dump($this->input->post());
					
					//if valid
					
					$title = $this->input->post('entry_name');
					$body = $this->input->post('entry_body');
					$categories = $this->input->post('entry_category');
					
					$this->Blog_model->add_new_entry($title,$body,$categories);
					$this->session->set_flashdata('message', '1 new post added!');
					redirect('Blog/add_new_entry');
				}
			    $this->render('demo/add_new_entry');
		}
		public function blog_info(){
			   
				$this->load->model('pagination_model');
				$config = array();
				$config["base_url"] = "http://[::1]/codingchallenge/Blog/blog_info";
				$total_row = $this->pagination_model->record_count();
				$config["total_rows"] = $total_row;
				$config["per_page"] = 2;
				$config['use_page_numbers'] = TRUE;
				$config['num_links'] = $total_row;
				$config['cur_tag_open'] = '&nbsp;<a class="current">';
				$config['cur_tag_close'] = '</a>';
				$config['next_link'] = 'Next';
				$config['prev_link'] = 'Previous';
				 $config["uri_segment"] = 3;
				 $config['page_query_string'] = TRUE;
				$this->pagination->initialize($config);
				if($this->uri->segment(3)){
				$page = ($this->uri->segment(3)) ;
				}
				else{
				$page = 1;
				}
				$pagination = $this->pagination->render($counts['total_num'], $counts['limit']);
				$this->mViewData['counts'] = $total_row;
				$this->mViewData['pagination'] = $pagination;
				$this->mViewData['results'] = $this->pagination_model->fetch_data($config["per_page"], $page);
				
				$str_links = $this->pagination->create_links();
				
				$this->mViewData["links"] = explode('&nbsp;',$str_links );
//print_r($data["links"]);
				// View data according to array.
				//$this->load->view("demo/blog_view", $data);
				$this->render('demo/blog_view');
}
		
		
}