<?php
Class Main extends MY_Controller {
	public function profile(){
		
		$this->render('demo/main');
	}
	 public function getSuggestedWebId()    {
            $firstName =  $_REQUEST['firstName'];
            $lastName =  $_REQUEST['lastName'];
			$data = array('firstName' => $firstName, 'lastName' => $lastName);
			$url = "http://im.gdbg.com:8080/commsapplib/ws/clSuggestWebId"; // where you want to post data
			$handle = curl_init($url);
			curl_setopt($handle, CURLOPT_POST, true);
			curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
			$result =curl_exec($handle);
			//var_dump($result); exit;// show output 
			curl_close ($handle); // close curl handle
			return $result ;    
        }
	 public function contacts(){
		
		$this->render('demo/contact');
	}	
	 public function getContactDetails(){
			$data = array('webId' => 'daryll');
			$url = "http://im.gdbg.com:8080/commsapplib/ws/clGetContactDetails"; // where you want to post data
			$handle = curl_init($url);
			curl_setopt($handle, CURLOPT_POST, true);
			curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
			$result =curl_exec($handle);
			//var_dump($result); exit;// show output 
			curl_close ($handle); // close curl handle
			return $result ;    
	 }	
	 
	public function jcarousel()
	{
		// grab records from database table "demo_cover_photos"
		$this->load->model('Blog_model');
		$this->mViewData['photos'] = $this->Blog_model->get_photos();
		$this->render('demo/jcarousel');
	}
	public function smartlife(){
		$this->render('demo/smartlife');
	}
}
?>